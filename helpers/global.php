<?php
function get_config($name)
{
    return include_once BASE_PATH . "configs" . DIRECTORY_SEPARATOR . "$name.php";
}

function site_url($uri)
{
    return BASE_URL . $uri;
}

function admin_url($uri)
{
    return site_url("admin/$uri");
}

function csrf_field()
{
    $token = bin2hex(random_bytes(16));
    $_SESSION['csrf_token'] = $token;
    return "<input type='hidden' name='csrf' value='$token' >";
}

function storage_url($uri)
{
    return BASE_URL . 'storage' . DIRECTORY_SEPARATOR . $uri;
}


function theme_path($file)
{
    return BASE_PATH . "views" . DIRECTORY_SEPARATOR . ACTIVE_THEME . $file;
}

function theme_asset($file)
{
    $path = "views/" . ACTIVE_THEME . "assets/";
    return site_url($path . $file);
}

function admin_asset($file)
{
    $path = "views" . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . "assets" . DIRECTORY_SEPARATOR;
    return site_url($path . $file);
}

function get_admin_partials($path)
{
    return BASE_PATH . "views/layouts/admin/$path.php";
}

function get_partials($path)
{
    return BASE_PATH . "views/theme/$path.php";
}

function remove_empty_params($arr)
{
    return array_filter($arr, function ($a) {
        return trim($a) !== "";
    });
}

function dd()
{
    echo '<pre>';
    array_map(function ($x) {
        var_dump($x);
    }, func_get_args());
    die;
}

function getFormField($attr)
{
    switch ($attr->type) {
        case 'tinytext':
            return "<input type='text' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'><br>";
            break;
        case 'bigtext':
            return "<textarea name='attr[$attr->id]' class='form-control' placeholder='$attr->title'></textarea><br>";
            break;
        case 'float':
            return "<input type='number'  step='0.01' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'><br>";
            break;
        case 'integer':
            return "<input type='number' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'><br>";
            break;
        default :
            return "<input type='text' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'><br>";
    }
}