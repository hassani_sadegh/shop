<div class="row"></div>
<div class=>
    <h4>مدیریت ویژگی ها</h4>
</div>

<div class="cat-wrapp col-md-10">
    <form action="<?= admin_url('attributes/create') ?>" method="post">
        <?= csrf_field() ?>
        <div class="form-group row">
            <label for="slug" class="col-sm-2 col-form-label label">نام ویژگی</label>
            <div class="col-sm-10">
                <input class="form-control" name="slug" type="text" id="slug" placeholder="حروف باید انگلیسی باشند">
            </div>
        </div>
        <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label label">عنوان ویژگی</label>
            <div class="col-sm-10">
                <input class="form-control" name="title" type="text" id="title"
                       placeholder="عنوانی برای ویژگی وارد کنید">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label label">دسته بندی والد</label>
            <div class="col-sm-10">
                <select class="custom-select" name="type">
                    <option value="integer">عددی</option>
                    <option value="float">اعشار</option>
                    <option value="tinytext">متن(رشته)</option>
                    <option value="bigtext">متن بزرگ</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-info waves-effect waves-light">ایجاد دسته بندی</button>
    </form>
</div>