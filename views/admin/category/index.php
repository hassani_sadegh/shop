<div class="row"></div>
<div class=>
    <h4>مدیریت دسته بندی</h4>
</div>

<div class="cat-wrapp col-md-10">
    <form action="<?= admin_url('categories/create') ?>" method="post">
        <?= csrf_field() ?>
        <div class="form-group row">
            <label for="slug" class="col-sm-2 col-form-label label">نام دسته یندی</label>
            <div class="col-sm-10">
                <input class="form-control" name="slug" type="text" id="slug" placeholder="حروف باید انگلیسی باشند">
            </div>
        </div>
        <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label label">عنوان دسته بندی</label>
            <div class="col-sm-10">
                <input class="form-control" name="title" type="text" id="title"
                       placeholder="عنوانی برای دسته بندی وارد کنید">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label label">دسته بندی والد</label>
            <div class="col-sm-10">
                <select class="custom-select" name="parent">
                    <option>انتخاب دسه بندی والد</option>
                    <?php foreach ($cats as $cat): ?>
                        <option value="<?php $cat->id ?>"> <?= $cat->title ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-info waves-effect waves-light">ایجاد دسته بندی</button>
    </form>
</div>
