<div class="row"></div>
<div class=>
    <h4>مدیریت ویژگی های دسته بندی</h4>
</div>

<div class="cat-wrapp col-md-10">
    <form action="<?= admin_url('categories/modify-attribute') ?>" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label label">دسته بندی</label>
            <div class="col-sm-10">
                <select class="custom-select" name="category_id" id="selectcat">
                    <option>انتخاب دسته بندی</option>
                    <?php foreach ($cats as $cat): ?>
                        <option value="<?= $cat->id ?>" <?php echo (isset($_GET['cat']) and $_GET['cat'] == $cat->id) ? "selected" : ""; ?>>
                            <?= $cat->title ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group col-md-10">
                <p>افزودن ویژگی</p>
                <?php foreach ($attrs as $attr): ?>
                    <label for="attr<?= $attr->id ?>" class="label-select-attr">
                        <input type="checkbox" class="checkbox-attr" value="<?= $attr->id ?>" name="selectedAttrs[]"
                               id="attr<?= $attr->id ?>"
                            <?= in_array($attr->id, $catAttrs) ? "checked" : "" ?>>
                        <?= $attr->title ?>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
        <button type="submit" class="btn btn-info waves-effect waves-light">انجام عملیات</button>
    </form>
</div>