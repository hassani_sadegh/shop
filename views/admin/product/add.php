<div class="row"></div>
<div class=>
    <h4>افزودن محصول</h4>
</div>
<div class="col-md-12 form-group" style="margin-top: 100px ;margin-bottom: 250px">
    <form action="">
        <div class="col-sm-10" style="margin-bottom: 15px">
            <select class="custom-select" name="parent" id="selectcat">
                <option>انتخاب دسه بندی</option>
                <?php foreach ($cats as $cat): ?>
                    <option value="<?= $cat->id ?>" <?php echo (isset($_GET['cat']) and $_GET['cat'] == $cat->id) ? "selected" : ""; ?>>
                        <?= $cat->title ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-sm-10" style="margin-bottom: 10px">
            <input class="form-control form-control-lg custom-input-product" type="text" name="title"
                   placeholder="اضافه کردن عنوان محصول">
        </div>
        <div class="col-sm-10" style="margin-top: 50px">
            <h4>توضیح محصول</h4>
            <textarea name="description" class="form-control" id="" cols="30" rows="10">
            </textarea>
        </div>
        <div class="col-sm-10" style="margin-top: 50px">
            <h4>ویژگی های محصول</h4>
            <div class="form-group">
                <?php foreach ($attrs as $attr): ?>
                    <?= getFormField($attr); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </form>
</div>
