<!-- ========== right Sidebar Start ========== -->
<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="mdi mdi-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">

            <a href="index-2.html" class="logo"><img src="<?= admin_asset('images/logo_dark.png') ?>" height="20"
                                                     alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">اصلی</li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i>
                        <span> داشبورد </span>
                        <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span>
                    </a>
                    <ul class="list-unstyled">
                        <li><a href="<?= site_url('admin') ?>">آمار و اطلاعات </a></li>
                    </ul>
                </li>
                <!-- category -->
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i>
                        <span> دسته بندی </span>
                        <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span>
                    </a>
                    <ul class="list-unstyled">
                        <li><a href="<?= admin_url('categories') ?>">مدیریت دسته بندی</a></li>
                        <li><a href="<?= admin_url('attributes') ?>">ویژگی ها</a></li>
                        <li><a href="<?= admin_url('categories/attributes') ?>">افزودن ویژگی به یک دسته بندی </a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i>
                        <span> محصولات </span>
                        <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span>
                    </a>
                    <ul class="list-unstyled">
                        <li><a href="<?= admin_url('product/list') ?>">لیست محصول</a></li>
                        <li><a href="<?= admin_url('product/add') ?>">افزودن محصول</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i>
                        <span> رسانه </span>
                        <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span>
                    </a>
                    <ul class="list-unstyled">
                        <li><a href="<?= admin_url('media/list') ?>">گالری تصاویر</a></li>
                        <li><a href="<?= admin_url('media/add') ?>">افزودن </a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
<!-- right Sidebar End -->