<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>زینزر - داشبورد ادمین بوت استرپ 4</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="<?= admin_asset('images/favicon.ico') ?>">
    <!-- Summernote css -->
    <link href="<?= admin_asset('plugins/summernote/summernote-bs4.css')?>" rel="stylesheet" />
    <!-- morris css -->
    <link rel="stylesheet" href="<?= admin_asset('plugins/morris/morris.css') ?>">

    <link href="<?= admin_asset('css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= admin_asset('css/icons.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= admin_asset('css/style.css') ?>" rel="stylesheet" type="text/css">


</head>


<body class="fixed-left">

<!-- Loader -->
<!--<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>-->

<!-- Begin page -->
<div id="wrapper">