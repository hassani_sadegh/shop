</div>
<!-- END wrapper -->
<!-- jQuery  -->
<script src="<?= admin_asset('js/jquery.min.js') ?>"></script>
<script src="<?= admin_asset('js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= admin_asset('js/modernizr.min.js') ?>"></script>
<script src="<?= admin_asset('js/detect.js') ?>"></script>
<script src="<?= admin_asset('js/fastclick.js') ?>"></script>
<script src="<?= admin_asset('js/jquery.slimscroll.js') ?>"></script>
<script src="<?= admin_asset('js/jquery.blockUI.js') ?>"></script>
<script src="<?= admin_asset('js/waves.js') ?>"></script>
<script src="<?= admin_asset('js/jquery.nicescroll.js') ?>"></script>
<script src="<?= admin_asset('js/jquery.scrollTo.min.js') ?>"></script>

<!--Morris Chart-->
<script src="<?= admin_asset('plugins/morris/morris.min.js') ?>"></script>
<script src="<?= admin_asset('plugins/raphael/raphael.min.js') ?>"></script>
<!-- summernote -->
<script src="<?= admin_asset('plugins/summernote/summernote-bs4.min.js') ?>"></script>

<!-- dashboard js -->
<script src="<?= admin_asset('pages/dashboard.int.js') ?>"></script>

<!-- App js -->
<script src="<?= admin_asset('js/app.js') ?>"></script>
<script src="<?= admin_asset('js/custom.js')?>"></script>
<script>
    jQuery(document).ready(function () {
        $('.summernote').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                 // set focus to editable area after initializing summernote
        });
    });
</script>
</body>
</html>