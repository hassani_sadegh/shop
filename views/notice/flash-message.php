<?php use App\Utilities\FlashMessage; ?>
<?php foreach ($flash_messages as $fm) : ?>
    <div class='flash-message <?= FlashMessage::get_css_class($fm->type) ?>'>
        <?= $fm->msg ?>
        <i class="fa fa-window-close close-btn"></i>
    </div>
<?php endforeach; ?>

