<footer>
    <!-- about-corporate-area start -->
    <div class="about-corporate-area bg-brown">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-0-sub">
                    <!-- single-corporate start -->
                    <div class="single-corporate">
                        <div class="corporate-icon">
                            <i class="ion-ios-timer-outline"></i>
                        </div>
                        <div class="corporate-text-des">
                            <h3>شنبه - پنجشنبه / 8 صبح - 6 عصر</h3>
                            <p>ساعات کاری</p>
                        </div>
                    </div>
                    <!-- single-corporate end -->
                </div>
                <div class="col-lg-4 col-md-4 col-0-sub">
                    <!-- single-corporate start -->
                    <div class="single-corporate">
                        <div class="corporate-icon">
                            <i class="ion-ios-telephone"></i>
                        </div>
                        <div class="corporate-text-des">
                            <h3><span class="ltr_text">+88 12 345 6789</span></h3>
                            <p>پشتیبانی رایگان!</p>
                        </div>
                    </div>
                    <!-- single-corporate end -->
                </div>
                <div class="col-lg-4 col-md-4 col-0-sub">
                    <!-- single-corporate start -->
                    <div class="single-corporate">
                        <div class="corporate-icon">
                            <i class="ion-email"></i>
                        </div>
                        <div class="corporate-text-des">
                            <h3>Support@gmail.com</h3>
                            <p>پشتیبانی سفارشات!</p>
                        </div>
                    </div>
                    <!-- single-corporate end -->
                </div>
            </div>
        </div>
    </div>
    <!-- about-corporate-area end -->

    <!-- footer-top start -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-title">
                        <h5>اطلاعات تماس</h5>
                    </div>
                    <div class="footer-content">
                        <p class="des">از طریق راه های ارتباطی زیر با ما در ارتباط باشید</p>
                        <ul class="footer-contact">
                            <li class="adress"><i class="ion-ios-location"></i>
                                <p>تبریز، خیابان ولیعصر، جنب بانک ملت، پلاک 896، طبقه 89</p></li>
                            <li class="phone"><i class="ion-ios-telephone"></i>
                                <p><span class="ltr_text">+123 456 789 ، +123 456 678</span></p></li>
                            <li class="email"><i class="ion-android-mail"></i>
                                <p>support@example.com</p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-title">
                        <h5>از بلاگ ما</h5>
                    </div>
                    <div class="home-blog">
                        <div class="articles-cont-active owl-carousel">
                            <div class="articles-container">
                                <div class="articles-inner">
                                    <div class="articles-image">
                                        <a href="#">
                                            <img src="<?= theme_asset('img/blog/s-1.jpg') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="aritcles-content">
                                        <a href="#">لورم ایپسوم متن ساختگی با تولید</a>
                                        <div class="articles-date">تیر <span>03</span></div>
                                    </div>
                                </div>
                                <div class="articles-inner">
                                    <div class="articles-image">
                                        <a href="#">
                                            <img src="<?= theme_asset('img/blog/s-2.jpg') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="aritcles-content">
                                        <a href="#">لورم ایپسوم متن ساختگی با تولید</a>
                                        <div class="articles-date">تیر <span>10</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="articles-container">
                                <div class="articles-inner">
                                    <div class="articles-image">
                                        <a href="#">
                                            <img src="<?= theme_asset('img/blog/s-1.jpg') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="aritcles-content">
                                        <a href="#">لورم ایپسوم متن ساختگی با تولید</a>
                                        <div class="articles-date">تیر <span>03</span></div>
                                    </div>
                                </div>
                                <div class="articles-inner">
                                    <div class="articles-image">
                                        <a href="#">
                                            <img src="<?= theme_asset('img/blog/s-2.jpg') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="aritcles-content">
                                        <a href="#">لورم ایپسوم متن ساختگی با تولید</a>
                                        <div class="articles-date">تیر <span>10</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-item-mg">
                    <div class="footer-title">
                        <h5>اطلاعات</h5>
                    </div>
                    <div class="footer-content">
                        <ul class="footer-list">
                            <li><a href="#">درباره ما</a></li>
                            <li><a href="#">نحوه ارسال</a></li>
                            <li><a href="#">حریم خصوصی </a></li>
                            <li><a href="#">قوانین و مقررات</a></li>
                            <li><a href="#">کارت های هدیه</a></li>
                            <li><a href="#">نمایندگی ها</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-item-mg">
                    <div class="footer-title">
                        <h5>خدمات مشتری</h5>
                    </div>
                    <div class="footer-content">
                        <ul class="footer-list">
                            <li><a href="#">تماس با ما</a></li>
                            <li><a href="#">بازگشت ها</a></li>
                            <li><a href="#">نقشه سایت</a></li>
                            <li><a href="#"> سابقه خرید</a></li>
                            <li><a href="#">علاقه‌مندی‌ها</a></li>
                            <li><a href="#"> خبرنامه</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-item-mg">
                    <div class="footer-title">
                        <h5>متفرقه</h5>
                    </div>
                    <div class="footer-content">
                        <ul class="footer-list">
                            <li><a href="#">برند ها</a></li>
                            <li><a href="#">کارت های هدیه</a></li>
                            <li><a href="#">نمایندگی ها </a></li>
                            <li><a href="#">محصولات ویژه</a></li>
                            <li><a href="#">حساب کاربری</a></li>
                            <li><a href="#">سابقه خرید</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-top end -->

    <!-- footer-bottom start -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="footer-copyright">
                        <p>ارائه شده در وب سایت <a href="https://www.rtl-theme.com">راست چین</a></p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer-payment">
                        <a href="#"><img src="<?= theme_asset('img/icon/payment.png') ?>" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom end -->

</footer>


<!-- Modal -->
<div class="modal fade modal-wrapper" id="exampleModalCenter">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-inner-area row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="single-product-tab">
                            <div class="zoomWrapper">
                                <div id="img-1" class="zoomWrapper single-zoom">
                                    <a href="#">
                                        <img id="zoom1" src="<?= theme_asset('img/product/1.jpg') ?>"
                                             data-zoom-image="img/product/1.jpg" alt="big-1">
                                    </a>
                                </div>
                                <div class="single-zoom-thumb">
                                    <ul class="s-tab-zoom single-product-active owl-carousel" id="gallery_01">
                                        <li>
                                            <a href="#" class="elevatezoom-gallery active" data-update=""
                                               data-image="img/product/1.jpg" data-zoom-image="img/product/1.jpg"><img
                                                        src="img/product/1.jpg" alt="zo-th-1"></a>
                                        </li>
                                        <li class="">
                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/2.jpg"
                                               data-zoom-image="img/product/2.jpg"><img src="img/product/2.jpg"
                                                                                        alt="zo-th-2"></a>
                                        </li>
                                        <li class="">
                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/3.jpg"
                                               data-zoom-image="img/product/3.jpg"><img src="img/product/3.jpg"
                                                                                        alt="ex-big-3"></a>
                                        </li>
                                        <li class="">
                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/4.jpg"
                                               data-zoom-image="img/product/4.jpg"><img src="img/product/4.jpg"
                                                                                        alt="zo-th-4"></a>
                                        </li>
                                        <li class="">
                                            <a href="#" class="elevatezoom-gallery" data-image="img/product/5.jpg"
                                               data-zoom-image="img/product/5.jpg"><img src="img/product/5.jpg"
                                                                                        alt="zo-th-5"></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <!-- product-thumbnail-content start -->
                        <div class="quick-view-content">
                            <div class="product-info">
                                <h2>لورم ایپسوم متن ساختگی</h2>
                                <div class="rating-box">
                                    <ul class="rating d-flex">
                                        <li><i class="ion-ios-star"></i></li>
                                        <li><i class="ion-ios-star"></i></li>
                                        <li><i class="ion-ios-star"></i></li>
                                        <li><i class="ion-android-star-outline"></i></li>
                                        <li><i class="ion-android-star-outline"></i></li>
                                    </ul>
                                </div>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان
                                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم</p>

                                <div class="price-box">
                                    <span class="new-price">25,000 تومان</span>
                                    <span class="old-price">30,000 تومان</span>
                                </div>

                                <ul class="list-unstyled">
                                    <li>برند: <a href="#">سامسونگ</a></li>
                                    <li>کد محصول: 254</li>
                                    <li>امتیاز: 4.5</li>
                                    <li>وضعیت: موجود</li>
                                </ul>

                                <div class="quick-add-to-cart">
                                    <form class="modal-cart">
                                        <div class="quantity">
                                            <label>تعداد</label>
                                            <div class="cart-plus-minus">
                                                <input type="number" value="1" min="0" step="1" class="input-box">
                                            </div>
                                        </div>
                                        <button class="add-to-cart" type="submit">افزودن به سبد</button>
                                        <ul class="wishlist-compare-btn">
                                            <li><a class="wishlist" href="#"><i class="ion-heart"></i> افزودن به
                                                    علاقه‌مندی‌ها</a></li>
                                            <li><a class="compare" href="#"><i class="ion-arrow-swap"></i> مقایسه</a>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- product-thumbnail-content end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>


<!-- jquery -->
<script src="<?= theme_asset('js/vendor/jquery-1.12.4.min.js') ?>"></script>
<!-- all plugins JS hear -->
<script src="<?= theme_asset('js/popper.min.js') ?>"></script>
<script src="<?= theme_asset('js/bootstrap.min.js') ?>"></script>
<script src="<?= theme_asset('js/owl.carousel.min.js') ?>"></script>
<script src="<?= theme_asset('js/jquery.mainmenu.js') ?>"></script>
<script src="<?= theme_asset('js/ajax-email.js') ?>"></script>
<script src="<?= theme_asset('js/plugins.js') ?>"></script>

<!-- main JS -->
<script src="<?= theme_asset('js/main.js') ?>"></script>
</body>
</html>