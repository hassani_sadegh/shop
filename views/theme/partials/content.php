<!-- slider-main-area start -->
<div class="slider-main-area pt-60 bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="slider-active-2 style-4 owl-carousel">
                    <!-- slider-wrapper start -->
                    <div class="slider-wrapper">
                        <div class="container">
                            <div class="row justify-content-end">
                                <img src="<?= theme_asset('img/slider/home-4-02.jpg') ?>" style="position: absolute"/>
                                <div class="col-lg-11 col-md-11">
                                    <div class="slider-text-info style-7 slider-text-animation">
                                        <h1 class="title-1">تلویزیون های</h1>
                                        <h2 class="title-2">سونی و سامسونگ</h2>
                                        <div class="slider-1-des">
                                            <p><span>82 اینچ</span> با کیفیت عالی</p>
                                        </div>
                                        <div class="slier-btn-1">
                                            <a title="shop now" href="#" class="shop-btn">هم اکنون بخرید</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- slider-wrapper end -->
                    <!-- slider-wrapper start -->
                    <div class="slider-wrapper">
                        <div class="container">
                            <div class="row justify-content-end">
                                <img src="<?= theme_asset('img/slider/home-4-01.jpg') ?>" style="position: absolute"/>
                                <div class="col-lg-6 col-md-6 col-9">
                                    <div class="slider-text-info style-8 slider-text-animation">
                                        <h2 class="title-2">آیفون 10 پلاس</h2>
                                        <div class="slider-1-des">
                                            <p>پیشنهاد ویژه <span>50% تخفیف</span></p>
                                        </div>
                                        <div class="slier-btn-1">
                                            <a title="shop now" href="#" class="shop-btn">هم اکنون بخرید</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- slider-wrapper end -->
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-lg-12 col-md-6">
                        <!-- single-banner start -->
                        <div class="single-banner m-mt-30">
                            <a href="#">
                                <img src="<?= theme_asset('img/banner/home-04-1.jpg') ?>" alt="">
                            </a>
                        </div>
                        <!-- single-banner end -->
                    </div>
                    <div class="col-lg-12 col-md-6">
                        <!-- single-banner start -->
                        <div class="single-banner mt-30">
                            <a href="#">
                                <img src="<?= theme_asset('img/banner/home-04-2.jpg') ?>" alt="">
                            </a>
                        </div>
                        <!-- single-banner end -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- slider-main-area end -->
<!-- section-row start -->
<div class="section-row bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- banner-area start -->
                <div class="banner-area pt-0">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <!-- single-banner start -->
                            <div class="single-banner mt-30">
                                <a href="#">
                                    <img src="<?= theme_asset('img/banner/home-01-1.jpg') ?>" alt="">
                                </a>
                            </div>
                            <!-- single-banner end -->
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <!-- single-banner start -->
                            <div class="single-banner mt-30">
                                <a href="#">
                                    <img src="<?= theme_asset('img/banner/home-01-2.jpg') ?>" alt="">
                                </a>
                            </div>
                            <!-- single-banner end -->
                        </div>
                    </div>
                </div>
                <!-- banner-area end -->


                <div class="row">
                    <div class="col-lg-8">
                        <!-- deals-offer-day start -->
                        <div class="deals-offer-day box-module">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="secton-title title-2">
                                        <h2>پیشنهاد های روز </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="deals-offer-one-active owl-carousel">
                                    <div class="col-lg-12">
                                        <!-- single-deals-offer start -->
                                        <div class="single-deals-offer  deals-offer-2">
                                            <div class="product-thumb">
                                                <a href="#" class="product-image"><img
                                                            src="<?= theme_asset('img/product/1.jpg') ?>"
                                                            alt=""></a>
                                                <div class="label-product label-sale ">-7%</div>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن ساختگی</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">80,000 تومان</span>
                                                    <span class="old-price">86,000 تومان</span>
                                                </div>
                                                <p class="text-hurryup">عجله کنید! مهلت پیشنهاد:</p>
                                                <!-- countdown start -->
                                                <div class="countdown-deals" data-countdown="2022/03/01"></div>
                                                <!-- countdown end -->
                                                <div class="action-links action-btn-orange">
                                                    <a href="#" class="btn-cart">افزودن به سبد</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-deals-offer end -->
                                    </div>
                                    <div class="col-lg-12">
                                        <!-- single-deals-offer start -->
                                        <div class="single-deals-offer deals-offer-2">
                                            <div class="product-thumb">
                                                <a href="#" class="product-image"><img
                                                            src="<?= theme_asset('img/product/9.jpg') ?>"
                                                            alt=""></a>
                                                <div class="label-product label-sale ">-7%</div>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن ساختگی</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">80,000 تومان</span>
                                                    <span class="old-price">86,000 تومان</span>
                                                </div>
                                                <p class="text-hurryup">عجله کنید! مهلت پیشنهاد:</p>
                                                <!-- countdown start -->
                                                <div class="countdown-deals" data-countdown="2022/03/01"></div>
                                                <!-- countdown end -->
                                                <div class="action-links action-btn-orange">
                                                    <a href="#" class="btn-cart">افزودن به سبد</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-deals-offer end -->
                                    </div>
                                    <div class="col-lg-12 deals-offer-2">
                                        <!-- single-deals-offer start -->
                                        <div class="single-deals-offer">
                                            <div class="product-thumb">
                                                <a href="#" class="product-image"><img
                                                            src="<?= theme_asset('img/product/6.jpg') ?>"
                                                            alt=""></a>
                                                <div class="label-product label-sale ">-7%</div>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن ساختگی</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">80,000 تومان</span>
                                                    <span class="old-price">86,000 تومان</span>
                                                </div>
                                                <p class="text-hurryup">عجله کنید! مهلت پیشنهاد:</p>
                                                <!-- countdown start -->
                                                <div class="countdown-deals" data-countdown="2022/03/01"></div>
                                                <!-- countdown end -->
                                                <div class="action-links action-btn-orange">
                                                    <a href="#" class="btn-cart">افزودن به سبد</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-deals-offer end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- deals-offer-day end -->
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <!--  box-product-area start -->
                        <div class="box-product-area mt-60">
                            <div class="product-module-1">
                                <div class="secton-title  title-2">
                                    <h2>برای شما</h2>
                                </div>
                                <div class="product-h-2 priduct-module-1-active owl-carousel">
                                    <div class="module-1-item">
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/2.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">102,000 تومان</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/1.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">80,000 تومان</span>
                                                    <span class="old-price">90,000 تومان</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/5.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">70,000 تومان</span>
                                                    <span class="old-price">80,000 تومان</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/7.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">120,000 تومان</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                    </div>
                                    <div class="module-1-item">
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/5.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/4.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/9.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/10.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                    </div>
                                    <div class="module-1-item">
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/5.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/4.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/9.jpg') ?>"
                                                                 alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                        <!-- single-product-module-1 start -->
                                        <div class="single-product-module-1">
                                            <div class="product-thumb">
                                                <a href="#"><img src="<?= theme_asset('img/product/10.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-desc">
                                                <h4 class="product-name"><a href="#">لورم ایپسوم متن</a></h4>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                    <span class="old-price">$122.00</span>
                                                </div>
                                                <div class="shopping-btn">
                                                    <a href="#" class="btn-cart"><i class="ion-ios-cart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-module-1 end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  box-product-area end -->
                    </div>
                </div>


                <!-- featured-categories-area start -->
                <div class="featured-categories-area box-module">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="secton-title  title-2">
                                <h2> دسته های ویژه</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 owl-container">
                            <div class="feategory-active owl-carousel">
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">زیبایی، سلامت و خانه‌داری</a></h3>
                                    </div>
                                </div>

                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">کتاب و مجله</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">دوربین</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">لوازم الکترونیکی</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">مد و جواهرات</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">مبلمان</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">هدفون</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">لپ تاپ و کامپیوتر</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                                <!-- fcategory-content start -->
                                <div class="fcategory-content">
                                    <div class="fcategory-image">
                                        <a href="#"><img src="<?= theme_asset('img/fcategory-img/1.png') ?>" alt=""></a>
                                    </div>
                                    <div class="fcategory-title">
                                        <h3><a href="#">موبایل</a></h3>
                                    </div>
                                </div>
                                <!-- fcategory-content end -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- featured-categories-area end -->

                <!-- product-area start -->
                <div class="row">
                    <div class="col-lg-3">
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <!-- single-banner start -->
                                <div class="single-banner mt-60 m-mt-30 text-center">
                                    <a href="#">
                                        <img src="<?= theme_asset('img/banner/home-04-4.jpg') ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- single-banner end -->
                            <div class="col-lg-12 col-md-6">
                                <!-- single-banner start -->
                                <div class="single-banner mt-30 text-center">
                                    <a href="#">
                                        <img src="<?= theme_asset('img/banner/home-04-5.jpg') ?>" alt="">
                                    </a>
                                </div>
                                <!-- single-banner end -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <!-- product-area start -->
                        <div class="product-area box-module">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs-categorys-list secton-title">
                                        <ul class="nav" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#new-arrivals" aria-controls="new-arrivals" role="tab"
                                                   data-toggle="tab">محبوب ترین ها</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#best-sellers" aria-controls="best-sellers" role="tab"
                                                   data-toggle="tab">پرفروش ها
                                                </a>
                                            </li>
                                            <li role="presentation"><a href="#on-sellers" aria-controls="on-sellers"
                                                                       role="tab" data-toggle="tab">جدیدترین ها</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- tab-contnt start -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="new-arrivals">
                                    <div class="product-wrapper ">
                                        <div class="row">
                                            <div class="prodict-active-4 owl-carousel">
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/25.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/24.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">$100.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/7.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">241,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/21.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">241,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/8.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">116,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/27.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">116,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/5.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/6.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/1.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/10.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="best-sellers">
                                    <div class="product-wrapper">
                                        <div class="row">
                                            <div class="prodict-active-4 owl-carousel">
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/1.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/10.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">$93.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/5.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">241,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/27.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">241,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/20.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">116,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/22.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">116,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/24.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/26.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="on-sellers">
                                    <div class="product-wrapper">
                                        <div class="row">
                                            <div class="prodict-active-4 owl-carousel">
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/1.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/10.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/28.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">241,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/23.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="label-product">-7%</div>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">241,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/5.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">116,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/15.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن ساختگی</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">116,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                                <div class="col-lg-3">
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/17.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                    <!-- sinle-product-item start -->
                                                    <div class="sinle-product-item mt-20">
                                                        <div class="product-thumb">
                                                            <a href="single-product.html">
                                                                <img class="primary-image"
                                                                     src="<?= theme_asset('img/product/22.jpg') ?>"
                                                                     alt="">
                                                            </a>
                                                            <div class="action-links action-btn-yellow">
                                                                <a class="action-btn btn-wishlist" href="#"><i
                                                                            class="ion-android-favorite-outline"></i></a>
                                                                <a class="action-btn btn-compare" href="#"><i
                                                                            class="ion-arrow-swap"></i></a>
                                                                <a class="action-btn btn-quickview" data-toggle="modal"
                                                                   data-target="#exampleModalCenter" href="#"><i
                                                                            class="ion ion-ios-eye"></i></a>
                                                            </div>
                                                            <div class="add-to-cart">
                                                                <a class="btn-cart btn-yellow" href="#"><span> افزودن به سبد </span></a>
                                                            </div>
                                                        </div>
                                                        <div class="product-info">
                                                            <div class="rating-box">
                                                                <ul class="rating d-flex">
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-ios-star"></i></li>
                                                                    <li><i class="ion-android-star-outline"></i></li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="product-name"><a href="single-product.html">لورم
                                                                    ایپسوم متن</a></h4>
                                                            <div class="price-box">
                                                                <span class="new-price">98,000 تومان</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sinle-product-item end -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- tab-contnt end -->
                        </div>
                        <!-- product-area end -->
                    </div>
                </div>
                <!-- product-area end -->

                <!-- product-area start -->
                <div class="product-area box-module">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="secton-title">
                                <h2>الکترونیک، کامپیوتر و اداری </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- single-banner start -->
                            <div class="single-banner mb-30">
                                <a href="#">
                                    <img src="<?= theme_asset('img/banner/home-01-4.jpg') ?>" alt="">
                                </a>
                            </div>
                            <!-- single-banner end -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="prodict-two-active owl-carousel">
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/5.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                ساختگی</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">98,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/9.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                ساختگی</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">241,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/22.jpg') ?>"
                                                 alt="">
                                            <img class="secondary-image" src="<?= theme_asset('img/product/6.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                ساختگی</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">116,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/7.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">98,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/8.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">98,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/9.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="label-product label-sale">-7%</div>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                ساختگی</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">80,000 تومان</span>
                                            <span class="new-price">86,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/10.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="label-product label-sale">-10%</div>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                ساختگی</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">80,000 تومان</span>
                                            <span class="new-price">86,000 تومان</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                            <div class="col-lg-3">
                                <!-- sinle-product-item start -->
                                <div class="sinle-product-item">
                                    <div class="product-thumb">
                                        <a href="single-product.html">
                                            <img class="primary-image" src="<?= theme_asset('img/product/11.jpg') ?>"
                                                 alt="">
                                        </a>
                                        <div class="action-links">
                                            <a class="action-btn btn-wishlist" href="#"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                            <a class="action-btn btn-compare" href="#"><i
                                                        class="ion-arrow-swap"></i></a>
                                            <a class="action-btn btn-quickview" data-toggle="modal"
                                               data-target="#exampleModalCenter" href="#"><i
                                                        class="ion ion-ios-eye"></i></a>
                                        </div>
                                        <div class="add-to-cart">
                                            <a class="btn-cart" href="#"><span> افزودن به سبد </span></a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <div class="rating-box">
                                            <ul class="rating d-flex">
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-ios-star"></i></li>
                                                <li><i class="ion-android-star-outline"></i></li>
                                            </ul>
                                        </div>
                                        <h4 class="product-name"><a href="single-product.html">لورم ایپسوم</a></h4>
                                        <div class="price-box">
                                            <span class="new-price">$210.00</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- sinle-product-item end -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- product-area end -->


                <!-- our-brand area start -->
                <div class="our-brand-area box-module">
                    <div class="brand-active owl-carousel">
                        <div class="single-brand-itme">
                            <a href="#"><img src="<?= theme_asset('img/brand/1.jpg') ?>" alt=""></a>
                        </div>
                        <div class="single-brand-itme">
                            <a href="#"><img src="<?= theme_asset('img/brand/2.jpg') ?>" alt=""></a>
                        </div>
                        <div class="single-brand-itme">
                            <a href="#"><img src="<?= theme_asset('img/brand/3.jpg') ?>" alt=""></a>
                        </div>
                        <div class="single-brand-itme">
                            <a href="#"><img src="<?= theme_asset('img/brand/4.jpg') ?>" alt=""></a>
                        </div>
                        <div class="single-brand-itme">
                            <a href="#"><img src="<?= theme_asset('img/brand/5.jpg') ?>" alt=""></a>
                        </div>
                    </div>
                </div>
                <!-- our-brand area end -->

                <!-- product-category-area start -->
                <div class="product-category-area">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <!-- camera-wrapper start -->
                            <div class="camera-wrapper box-module">
                                <div class="secton-title">
                                    <h2>دوربین</h2>
                                </div>
                                <!-- product-category-active start -->
                                <div class="product-category-active owl-carousel">
                                    <div class="product-category-inner ">
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/2.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/4.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                        ساختگی</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">198,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/8.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                    </div>
                                    <div class="product-category-inner ">
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/14.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">198,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/2.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                    </div>
                                </div>
                                <!-- product-category-active end -->
                            </div>
                            <!-- camera-wrapper end -->
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <!-- tablet-accessories-wrapper start -->
                            <div class="tablet-accessories-wrapper box-module">
                                <div class="secton-title">
                                    <h2>تبلت و لوازم جانبی</h2>
                                </div>
                                <!-- product-category-active start -->
                                <div class="product-category-active owl-carousel">
                                    <div class="product-category-inner ">
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/7.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/9.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم متن
                                                        ساختگی</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">198,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/10.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                    </div>
                                    <div class="product-category-inner ">
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/10.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">198,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                        <!-- product-list-style start -->
                                        <div class="product-list-style">
                                            <div class="product-thumb">
                                                <a href="single-product.html"><img
                                                            src="<?= theme_asset('img/product/9.jpg') ?>" alt=""></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="rating-box">
                                                    <ul class="rating d-flex">
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-ios-star"></i></li>
                                                        <li><i class="ion-android-star-outline"></i></li>
                                                    </ul>
                                                </div>
                                                <h3 class="product-name"><a href="single-product.html">لورم ایپسوم
                                                        متن</a></h3>
                                                <div class="price-box">
                                                    <span class="new-price">98,000 تومان</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- product-list-style end -->
                                    </div>
                                </div>
                                <!-- product-category-active end -->
                            </div>
                            <!-- tablet-accessories-wrapper end -->
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <!-- signup-newsletter-wrapper start -->
                            <div class="signup-newsletter-wrapper box-module">
                                <div class="secton-title">
                                    <h2>در خبرنامه ما عضو شوید</h2>
                                </div>
                                <div class="product-category-inner">
                                    <label>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از
                                        طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که
                                        لازم</label>
                                    <div class="box-content newleter-content">
                                        <form id="subscribe-normal" name="subscribe">
                                            <input class="subscribe-email" type="text"
                                                   placeholder="ایمیل خود را وارد کنید ...">
                                        </form>
                                        <a class="subscribe-btn" href="#">اشتراک</a>
                                    </div>
                                    <p class="text-dontspam">ما به شما اسپم نخواهیم فرستاد!</p>
                                    <div class="social-follow">
                                        <div class="secton-title">
                                            <h2>ما را در شبکه های اجتماعی دنبال کنید</h2>
                                        </div>
                                        <ul class="social-link-follow">
                                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#" class="Google"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#" class="Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#" class="telegram"><i class="fa fa-telegram"></i></a></li>
                                            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- signup-newsletter-wrapper end -->
                        </div>
                    </div>
                </div>
                <!-- product-category-area start -->
            </div>
        </div>
    </div>
</div>
<!-- section-row end -->
