<!-- header-area start -->
<div class="header-area bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <!-- logo start -->
                <div class="logo">
                    <a href="<?= site_url('') ?>">
                        <img src="<?= theme_asset('img/logo/logo-4.png') ?>" alt=""></a>
                </div>
                <!-- logo end -->
            </div>
            <div class="col-lg-6 col-md-6">
                <!-- top-search-area start -->
                <div class="top-search-area shearch-fore">
                    <div class="search-categories">
                        <form action="#">
                            <div class="search-form-input">
                                <select id="select" name="select" class="nice-select">
                                    <option value="">تمامی دسته ها</option>
                                    <option value="12">بدون دسته</option>
                                    <option value="22">لوازم الکترونیکی</option>
                                    <option value="26">لوازم جانبی</option>
                                    <option value="27">کابل HDMI</option>
                                    <option value="28">هدفون</option>
                                    <option value="29">کیبورد</option>
                                    <option value="23">ماوس</option>
                                    <option value="30">لپ تاپ و تبلت</option>
                                    <option value="31">لپ تاپ</option>
                                    <option value="31">مک بوک</option>
                                    <option value="31">گوشی هوشمند</option>
                                    <option value="31">تبلت ها</option>
                                    <option value="32">صوتی و تصویری</option>
                                    <option value="33">تقویت کننده</option>
                                    <option value="24">تلویزیون هوشمند</option>
                                    <option value="34">اسپیکر</option>
                                    <option value="35">تلویزیون</option>
                                    <option value="36">مد و جواهرات</option>
                                    <option value="37">لوازم جانبی</option>
                                    <option value="25">حلقه ها</option>
                                    <option value="38">ساعت ها</option>
                                </select>
                                <input type="text" placeholder="جستجوی محصول ...">
                                <button class="top-search-btn" type="submit"><i class="ion-ios-search-strong"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- top-search-area end -->
            </div>
            <div class="col-lg-3 col-md-3">
                <!-- header-cart-box start -->
                <div class="header-cart-box header-cart-box-fore">
                    <!-- setting-account start -->
                    <div class="setting-account">
                        <div class="btn-group">
                            <button class="btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                حساب کاربری <i class="fa fa-angle-down"></i>
                            </button>
                            <div class="dropdown-menu">
                                <ul>
                                    <li><a href="login-register.html">ثبت نام</a></li>
                                    <li><a href="login-register.html">ورود</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- setting-account end -->
                    <!-- top-shopoing-cart start -->
                    <div id="top-shopoing-cart" class="btn-group">
                        <button class="btn-link shopping-cart shopping-cart-fore dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="ion-ios-cart"></i>
                            <span class="top-cart-total">
                                                <span class="item-text-number">2</span>
                                                <span class="cart-text-items">سبد خرید</span>
                                            </span>
                        </button>
                        <div class="dropdown-menu">
                            <ul class="mini-cart-sub mini-cart-fore">
                                <li class="single-cart">
                                    <div class="cart-img">
                                        <a href="single-product.html">
                                            <img src="<?= theme_asset('img/small-product/1.jpg') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="cart-info">
                                        <a href="single-product.html">لورم ایپسوم متن ساختگی</a>
                                        <p class="cart-quantity">×1</p>
                                        <p class="cart-price">78,000 تومان</p>
                                    </div>
                                    <button class="cart-remove" title="Remove"><i class="ion-ios-close-empty"></i>
                                    </button>
                                </li>
                                <li class="single-cart">
                                    <div class="cart-img">
                                        <a href="#">
                                            <img src="<?= theme_asset('img/small-product/2.jpg') ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="cart-info">
                                        <a href="single-product.html">لورم ایپسوم متن</a>
                                        <p class="cart-quantity">×1</p>
                                        <p class="cart-price">242,000 تومان</p>
                                    </div>
                                    <button class="cart-remove" title="Remove"><i class="ion-ios-close-empty"></i>
                                    </button>
                                </li>
                                <li class="cart-total-box">
                                    <h5>مجموع:<span class="float-right">264,000 تومان</span></h5>
                                    <h5>مالیات:<span class="float-right">4,000 تومان</span></h5>
                                    <h5>تخفیف:<span class="float-right">52,000 تومان</span></h5>
                                    <h5>جمع:<span class="float-right">320,000 تومان</span></h5>
                                </li>
                                <li>
                                    <p class="text-center cart-button">
                                        <a href="cart.html">مشاهده سبد</a>
                                        <a href="checkout.html">پرداخت</a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- top-shopoing-cart end -->
                </div>
                <!-- header-cart-box end -->
            </div>
        </div>
    </div>
</div>
<!-- header-area end -->