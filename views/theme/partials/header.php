<!DOCTYPE html>
<html class="no-js" lang="fa">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>قالب فروشگاهی Palora</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/png" href="<?= theme_asset('img/favicon.png') ?>">
    <!-- all CSS hear -->
    <link rel="stylesheet" href="<?= theme_asset('css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/ionicons.min.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/animate.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/nice-select.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/jquery-ui.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/owl.carousel.min.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/mainmenu.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/style.css') ?>">
    <link rel="stylesheet" href="<?= theme_asset('css/responsive.css') ?>">
    <script src="<?= theme_asset('js/vendor/modernizr-2.8.3.min.js') ?>"></script>
</head>
<body>

<div class="wrapper home-4">
    <header>