<!-- main-menu-area start -->
<div class="main-menu-area sticky-header bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 d-none d-lg-block">
                <div class="all-categories-menu-area categories-style-fore">
                    <div class="categories-toggler-menu togoler-style-4">
                        <i class="icon-left ion-navicon-round"></i>
                        <span> تمامی دسته ها </span>
                        <i class="icon-right ion-android-arrow-dropdown"></i>
                    </div>
                    <div class="vertical-menu-list">
                        <ul>
                            <li class="has-sub"><a href="#">کتاب و مجله <span><i
                                                class="fa fa-angle-left"></i></span></a>
                                <ul class="right-sub-menu">
                                    <li class="has-sub"><a href="#">چکمه ها <span><i
                                                        class="fa fa-angle-left"></i></span></a>
                                        <ul class="right-threed-sub">
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">عرق گیر</a></li>
                                            <li><a href="#">جلیقه</a></li>
                                            <li><a href="#">قلم</a></li>
                                        </ul>
                                    </li>
                                    <li class="has-sub"><a href="#">حلقه ها <span><i
                                                        class="fa fa-angle-left"></i></span></a>
                                        <ul class="right-threed-sub">
                                            <li><a href="#">روزانه</a></li>
                                            <li><a href="#">چینی</a></li>
                                            <li><a href="#">پیاده روی</a></li>
                                            <li><a href="#">سفارشی</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="#">فیلم، موزیک و بازی <span><i
                                                class="fa fa-angle-left"></i></span></a>
                                <ul class="right-mega-menu">
                                    <li><a href="#">دوربین</a>
                                        <ul>
                                            <li><a href="#">آستین بلند</a></li>
                                            <li><a href="#">آستین کوتاه مجلسی</a></li>
                                            <li><a href="#">آستین کوتاه</a></li>
                                            <li><a href="#">بدون آستین</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">هدفون</a>
                                        <ul>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">کت و ژاکت</a></li>
                                            <li><a href="#">عرق گیر</a></li>
                                            <li><a href="#">تاپ و تی شرت</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">موبایل</a>
                                        <ul>
                                            <li><a href="#">تی شرت گرافیکی</a></li>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">آستین کوتاه مجلسی</a></li>
                                            <li><a href="#">بدون آستین</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">تبلت و لوازم جانبی</a>
                                        <ul>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">عرق گیر</a></li>
                                            <li><a href="#">جلیقه</a></li>
                                            <li><a href="#">قلم</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="#">الکترونیک، کامپیوتر و اداری <span><i
                                                class="fa fa-angle-left"></i></span></a>
                                <ul class="right-mega-menu">
                                    <li><a href="#">لپ تاپ و کامپیوتر</a>
                                        <ul>
                                            <li><a href="#">لوازم جانبی</a></li>
                                            <li><a href="#">کامپیوتر و تبلت</a></li>
                                            <li><a href="#">درایو ذخیره سازی</a></li>
                                            <li><a href="#">مانیتور</a></li>
                                            <li><a href="#">ابزار شبکه</a></li>
                                            <li><a href="#">پرینتر و جوهر</a></li>
                                            <li><a href="#">نرم افزار</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">لوازم الکترونیکی</a>
                                        <ul>
                                            <li><a href="#">اسپیکر بی سیم</a></li>
                                            <li><a href="#">دوربین، عکس و فیلم</a></li>
                                            <li><a href="#">تلفن همراه و لوازم جانبی</a></li>
                                            <li><a href="#">هدفون</a></li>
                                            <li><a href="#">سینمای خانگی</a></li>
                                            <li><a href="#">تلویزیون و ویدئو</a></li>
                                            <li><a href="#">بازی های ویدیویی</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-banner-image">
                                        <a href="shop.html">
                                            <img src="<?= theme_asset('img/banner/img-static-menu.jpg') ?>" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="#">خانه، باغچه، حیوانات و ابزار <span><i
                                                class="fa fa-angle-left"></i></span></a>
                                <ul class="right-mega-menu">
                                    <li><a href="#">مخلوط کن ها</a>
                                        <ul>
                                            <li><a href="#">تی شرت گرافیکی</a></li>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">آستین کوتاه مجلسی</a></li>
                                            <li><a href="#">بدون آستین</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">مخلوط کن الکتریکی</a>
                                        <ul>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">کت و ژاکت</a></li>
                                            <li><a href="#">عرق گیر</a></li>
                                            <li><a href="#">تاپ و تی شرت</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">پردازش غذا</a>
                                        <ul>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">عرق گیر</a></li>
                                            <li><a href="#">جلیقه</a></li>
                                            <li><a href="#">قلم</a></li>
                                        </ul>

                                    </li>
                                    <li><a href="#">تاپ و تی شرت</a>
                                        <ul>
                                            <li><a href="#">تی شرت گرافیکی</a></li>
                                            <li><a href="#">پیراهن</a></li>
                                            <li><a href="#">آستین کوتاه مجلسی</a></li>
                                            <li><a href="#">بدون آستین</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="#">غذا و مواد غذایی <span><i
                                                class="fa fa-angle-left"></i></span></a>
                                <ul class="right-sub-menu">
                                    <li class="has-sub"><a href="#">ماهی تابه ها <span><i class="fa fa-angle-left"></i></span></a>
                                        <ul class="right-threed-sub">
                                            <li><a href="#">روزانه</a></li>
                                            <li><a href="#">چینی</a></li>
                                            <li><a href="#">پیاده روی</a></li>
                                            <li><a href="#">سفارشی</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#">زیبایی، سلامت و خانه‌داری</a></li>
                            <li><a href="#">اسباب بازی و کودکان</a></li>
                            <li><a href="#">مد و جواهرات</a></li>
                            <li><a href="#">صنایع دستی</a></li>
                            <li><a href="#">ورزش و گردش</a></li>
                            <li class="hide-child"><a href="shop.html">مبلمان</a></li>
                            <li class="categories-more-less ">
                                <a class="more-default"><span class="c-more"></span>+ آیتم های بیشتر</a>
                                <a class="less-show"><span class="c-more"></span>- بستن آیتم ها</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="main-menu d-none d-lg-block">
                    <nav>
                        <ul>
                            <li><a href="index.html">خانه</a>
                                <ul class="dorpdown-menu">
                                    <li><a href="index.html">صفحه خانه 1</a></li>
                                    <li><a href="index-2.html">صفحه خانه 2</a></li>
                                    <li><a href="index-3.html">صفحه خانه 3</a></li>
                                    <li><a href="index-4.html">صفحه خانه 4</a></li>
                                </ul>
                            </li>
                            <li><a href="shop.html">صفحات</a>
                                <ul class="mega-menu">
                                    <li><a href="#">ستون یک</a>
                                        <ul>
                                            <li><a href="my-account.html">حساب کاربری</a></li>
                                            <li><a href="frequently-question.html">سوالات متداول</a></li>
                                            <li><a href="login-register.html">ورود و ثبت نام</a></li>
                                            <li><a href="error404.html">خطای 404</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">ستون دو</a>
                                        <ul>
                                            <li><a href="blog-right.html">بلاگ با نواری کناری راست</a></li>
                                            <li><a href="blog.html">بلاگ با نوار کناری چپ</a></li>
                                            <li><a href="blog-fullwidth.html">بلاگ تمام عرض</a></li>
                                            <li><a href="blog-details.html">جزئیات بلاگ</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">ستون سه</a>
                                        <ul>
                                            <li><a href="shop.html">فروشگاه با نوار کناری راست</a></li>
                                            <li><a href="shop-left.html">فروشگاه با نوار کناری چپ</a></li>
                                            <li><a href="shop-fullwidth.html">فروشگاه تمام عرض</a></li>
                                            <li><a href="wishlist.html">علاقه‌مندی‌ها</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="blog.html">بلاگ</a></li>
                            <li><a href="shop.html">فروشگاه</a></li>
                            <li><a href="about.html">درباره ما</a></li>
                            <li><a href="contact.html">تماس با ما</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="top-box-lang top-box-lang-4 text-right">
                    <ul>
                        <li class="currency">
                            <div class="btn-group">
                                <button class="btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    $ دلار آمریکا <i class="fa fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li><a href="#">€ یورو</a></li>
                                        <li><a href="#">£ پوند</a></li>
                                        <li><a href="#">$ دلار آمریکا</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="language">
                            <div class="btn-group">
                                <button class="btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    <img src="<?= theme_asset('img/icon/en-gb.png') ?>" alt=""> انگلیسی <i
                                            class="fa fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img src="<?= theme_asset('img/icon/en-gb.png') ?>" alt="">انگلیسی
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="<?= theme_asset('img/icon/de-de.png') ?>" alt="">آلمانی</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Mobile Menu Area Start -->
        <div class="mobile-menu-area d-block d-lg-none">
            <div class="mobile-menu">
                <nav id="mobile-menu-active">
                    <ul>
                        <li><a href="index.html">خانه</a>
                            <ul>
                                <li><a href="index.html">صفحه خانه 1</a></li>
                                <li><a href="index-2.html">صفحه خانه 2</a></li>
                                <li><a href="index-3.html">صفحه خانه 3</a></li>
                                <li><a href="index-4.html">صفحه خانه 4</a></li>
                            </ul>
                        </li>
                        <li><a href="shop.html">صفحات</a>
                            <ul>
                                <li><a href="#">ستون یک</a>
                                    <ul>
                                        <li><a href="my-account.html">حساب کاربری</a></li>
                                        <li><a href="frequently-question.html">سوالات متداول</a></li>
                                        <li><a href="login-register.html">ورود و ثبت نام</a></li>
                                        <li><a href="error404.html">خطای 404</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">ستون دو</a>
                                    <ul>
                                        <li><a href="blog-right.html">بلاگ با نواری کناری راست</a></li>
                                        <li><a href="blog.html">بلاگ با نوار کناری چپ</a></li>
                                        <li><a href="blog-fullwidth.html">بلاگ تمام عرض</a></li>
                                        <li><a href="blog-details.html">جزئیات بلاگ</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">ستون سه</a>
                                    <ul>
                                        <li><a href="shop.html">فروشگاه با نوار کناری راست</a></li>
                                        <li><a href="shop-left.html">فروشگاه با نوار کناری چپ</a></li>
                                        <li><a href="shop-fullwidth.html">فروشگاه تمام عرض</a></li>
                                        <li><a href="wishlist.html">علاقه‌مندی‌ها</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="blog.html">بلاگ</a></li>
                        <li><a href="shop.html">فروشگاه</a></li>
                        <li><a href="about.html">درباره ما</a></li>
                        <li><a href="contact.html">تماس با ما</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- Mobile Menu Area End -->
    </div>
</div>
<!-- main-menu-area end -->
<!-- mobile-categorei-menu start -->
<div class="container d-block d-lg-none">
    <div class="vertical-menu mt-10 pb-30">
        <span class="categorie-title mobile-categorei-menu">تمامی دسته ها</span>
        <nav>
            <div class="category-menu sidebar-menu sidbar-style mobile-categorei-menu-list menu-hidden "
                 id="cate-mobile-toggle">
                <ul>
                    <li class="has-sub"><a href="#">کتاب و مجله </a>
                        <ul class="category-sub">
                            <li class="has-sub"><a href="shop.html">چکمه ها</a>
                                <ul class="category-sub">
                                    <li><a href="shop.html">پیراهن</a></li>
                                    <li><a href="shop.html">عرق گیر</a></li>
                                    <li><a href="shop.html">جلیقه</a></li>
                                    <li><a href="shop.html">قلم</a></li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="shop.html">حلقه ها</a>
                                <ul class="category-sub">
                                    <li><a href="shop.html">روزانه</a></li>
                                    <li><a href="shop.html">چینی </a></li>
                                    <li><a href="shop.html">پیاده روی</a></li>
                                    <li><a href="shop.html">سفارشی</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- category submenu end-->
                    </li>
                    <li class="has-sub"><a href="#">فیلم، موزیک و بازی </a>
                        <ul class="category-sub">
                            <li class="menu-tile">دوربین</li>
                            <li><a href="shop.html">آستین بلند</a></li>
                            <li><a href="shop.html">آستین کوتاه مجلسی</a></li>
                            <li><a href="shop.html">آستین کوتاه</a></li>
                            <li><a href="shop.html">بدون آستین</a></li>
                        </ul>
                        <!-- category submenu end-->
                    </li>
                    <li class="has-sub"><a href="#">هدفون</a>
                        <ul class="category-sub">
                            <li><a href="shop.html">پیراهن</a></li>
                            <li><a href="shop.html">کت و ژاکت</a></li>
                            <li><a href="shop.html">عرق گیر</a></li>
                            <li><a href="shop.html">لورم ایپسوم متن</a></li>
                        </ul>
                        <!-- category submenu end-->
                    </li>
                    <li class="has-sub"><a href="#">موبایل</a>
                        <ul class="category-sub">
                            <li><a href="shop.html">موبایل</a></li>
                            <li><a href="shop.html">تی شرت گرافیکی</a></li>
                            <li><a href="shop.html">پیراهن</a></li>
                            <li><a href="shop.html">آستین کوتاه مجلسی</a></li>
                            <li><a href="shop.html">بدون آستین</a></li>
                        </ul>
                        <!-- category submenu end-->
                    </li>
                    <li class="has-sub"><a href="#">تبلت و لوازم جانبی</a>
                        <ul class="category-sub">
                            <li><a href="shop.html">پیراهن</a></li>
                            <li><a href="shop.html">عرق گیر</a></li>
                            <li><a href="shop.html">جلیقه</a></li>
                            <li><a href="shop.html">قلم</a></li>
                        </ul>
                        <!-- category submenu end-->
                    </li>
                    <li class="has-sub"><a href="#">خانه، باغچه، حیوانات و ابزار </a>
                        <ul class="category-sub">
                            <li class="has-sub"><a href="shop.html">چکمه ها</a>
                                <ul class="category-sub">
                                    <li><a href="shop.html">پیراهن</a></li>
                                    <li><a href="shop.html">عرق گیر</a></li>
                                    <li><a href="shop.html">جلیقه</a></li>
                                    <li><a href="shop.html">قلم</a></li>
                                </ul>
                            </li>
                            <li class="has-sub"><a href="shop.html">حلقه ها</a>
                                <ul class="category-sub">
                                    <li><a href="shop.html">روزانه</a></li>
                                    <li><a href="shop.html">چینی </a></li>
                                    <li><a href="shop.html">پیاده روی</a></li>
                                    <li><a href="shop.html">سفارشی</a></li>
                                </ul>
                            </li>
                        </ul>
                        <!-- category submenu end-->
                    </li>
                    <li><a href="#">مد و جواهرات</a></li>
                    <li><a href="#">ورزش و گردش</a></li>
                </ul>
            </div>
            <!-- category-menu-end -->
        </nav>
    </div>
</div>
<!-- mobile-categorei-menu end -->