<?php


namespace App\Controllers\Admin;


use App\Core\Request;
use App\Models\Attribute;
use App\Models\Category;
use App\Services\View\View;
use App\Utilities\FlashMessage;

class CategoryController
{
    private $catModel;
    private $attrModel;

    public function __construct()
    {
        $this->catModel = new Category();
        $this->attrModel = new Attribute();
    }

    public function index(Request $request)
    {
        $data = array(
            'cats' => $this->catModel->all(),
        );
        View::Base('admin.category.index', $data, 'admin.admin-layout');
    }

    public function create(Request $request)
    {
        $already_slug = $this->catModel->findBy('slug', ['slug' => $request->slug]);
        if ($already_slug) {
            FlashMessage::add('این دسته بندی وجود دارد', FlashMessage::INFO);
            Request::redirect(admin_url('categories'));
            die();
        }

        $create_cat = $this->catModel->create($request->except(['csrf']));
        if ($create_cat) {
            FlashMessage::add('عملیات با موفقیت انجام شد', FlashMessage::SUCCESS);
            Request::redirect(admin_url('categories'));
        }
    }

    public function attribute(Request $request)
    {
        $data = array(
            'cats' => $this->catModel->all(),
            'attrs' => $this->attrModel->all(),
            'catAttrs' => []
        );

        if (isset($_GET['cat']) && is_numeric($_GET['cat'])) {
            $data['catAttrs'] = $this->catModel->getAttributeIds($_GET['cat']);
        }
        View::Base('admin.category.attribute', $data, 'admin.admin-layout');
    }

    public function modify_attributes(Request $request)
    {
        $validation = false;
        if ($validation) {
            FlashMessage::add('خطا در انجام عملیات', FlashMessage::ERROR);
            Request::redirect(admin_url("categories/attributes?cat=$request->category_id"));
        } else {
            $this->catModel->setAttributes($request->param('category_id'), $request->param('selectedAttrs'));
            FlashMessage::add('عملیات با موفقیت انجام شد', 1);
            Request::redirect(admin_url("categories/attributes?cat=$request->category_id"));
        }
    }
}

