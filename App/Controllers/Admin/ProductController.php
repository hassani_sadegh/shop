<?php


namespace App\Controllers\Admin;


use App\Core\Request;
use App\Models\Category;
use App\Models\Product;
use App\Services\View\View;

class ProductController
{

    private $productModel;
    private $catModel;

    public function __construct()
    {
        $this->productModel = new Product();
        $this->catModel = new Category();
    }

    public function list(Request $request)
    {
        $data = array();
        View::Base('admin.product.list', $data, 'admin.admin-layout');
    }

    public function add(Request $request)
    {
        $data = array(
            'cats' => $this->catModel->all(),
            'attrs' => []
        );
        if (isset($_GET['cat']) and is_numeric($_GET['cat'])){
            $data['attrs'] = $this->catModel->getAttributeName($_GET['cat']);
        }
        View::Base('admin.product.add', $data, 'admin.admin-layout');
    }

}