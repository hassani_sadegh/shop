<?php


namespace App\Controllers\Admin;


use App\Core\Request;
use App\Models\Attribute;
use App\Services\View\View;
use App\Utilities\FlashMessage;

class AttributeController
{
    private $attModel;

    public function __construct()
    {
        $this->attModel = new Attribute();
    }

    public function index(Request $request)
    {
        $data = array();
        View::Base('admin.attribute.index', $data, 'admin.admin-layout');
    }

    public function create(Request $request)
    {
        // operation validation
        $already_slug = $this->attModel->findBy('slug', ['slug' => $request->slug]);
        if ($already_slug) {
            FlashMessage::add('این ویژگی قبلا ایجاد شده است', FlashMessage::INFO);
            Request::redirect(admin_url('attributes'));
            die();
        }

        // operation create
        $create_cat = $this->attModel->create($request->except(['csrf']));
        if ($create_cat) {
            FlashMessage::add('عملیات با موفقیت انجام شد', FlashMessage::SUCCESS);
            Request::redirect(admin_url('attributes'));
        }
    }
}