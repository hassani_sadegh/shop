<?php


namespace App\Controllers\Admin;


use App\Core\Request;
use App\Services\View\View;

class AdminController
{
    public function index(Request $request)
    {
        $data = array();
        View::Base('admin.dashboard',$data,'admin.admin-layout');
    }
}