<?php


namespace App\Utilities;


class Presenter
{
    public static function excerpt($content, $num)
    {
        $word = explode(" ", $content);
        return implode(" ", array_slice($word, 0, $num)) . "...";
    }
}