<?php

namespace App\Core;

class Uploader
{
    private $file;
    private $file_name;

    public function __construct($file_name, $sub_folder = null)
    {
        $this->file = $_FILES[$file_name];
        if ($sub_folder == null) {
            $sub_folder = date("Y-m");
            $sub_folder_path = STORAGE . $sub_folder;
            if (!file_exists($sub_folder_path)) {
                mkdir($sub_folder_path);
            }
        }
        $this->file_name = $sub_folder . DIRECTORY_SEPARATOR . $this->basename() . "-" . $this->generate_random_str() . $this->extension();
    }

    public function name()
    {
        return substr($this->file['name'], 0, 32);
    }

    public function extension()
    {
        $arr = explode(".", $this->name());
        return '.' . end($arr);
    }

    public function basename()
    {
        return basename($this->name(), $this->extension());
    }

    private function generate_random_str()
    {
        return bin2hex(random_bytes(3));
    }

    private function upload($path)
    {
        return move_uploaded_file($this->file['tmp_name'], $path);
    }

    private function delete()
    {
        $path = STORAGE . $this->file_name;
        if (!file_exists($path)) {
            return;
        }
        return rename($path, "$path.deleted");
    }

    public function save()
    {
        $path = STORAGE . $this->file_name;

        if ($this->upload($path)) {
            return storage_url($this->file_name);
        } else {
            return false;
        }

    }
}

