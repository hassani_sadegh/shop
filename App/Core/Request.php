<?php

namespace App\Core;

class Request
{
    public $ip;
    public $method;
    public $referer;
    public $agent;
    public $uri;
    public $params;
    public $file;

    public function __construct()
    {
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->referer = $_SERVER['HTTP_REFERER'] ?? "";
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->params = $_REQUEST;
        $this->file = $_FILES;
    }

    public function param($key)
    {
        return $this->params[$key];
    }

    public function is_in($method_arr)
    {
        return in_array($this->method, $method_arr);
    }

    public function only($keys)
    {
        if (empty($keys)) {
            return [];
        }

        $only = array_filter($this->params, function ($k) use ($keys) {
            if (in_array($k, $keys)) {
                return true;
            }
        }, ARRAY_FILTER_USE_KEY);

        return $only;
    }

    public function except($arr)
    {
        if (empty($arr)) {
            return $this->params;
        }

        $arr = array_filter($this->params, function ($k) use ($arr) {
            if (!in_array($k, $arr)) {
                return true;
            }
        }, ARRAY_FILTER_USE_KEY);
        return $arr;
    }

    public function key_exist($key)
    {
        return in_array($key, array_keys($this->params));
    }

    public function __get($key)
    {
        if ($this->key_exist($key)) {
            return $this->{$key} = $this->param($key);
        } else {
            echo "property {$key} not exist";
        }
    }

    public function is_ajax()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        } else {
            return false;
        }
    }

    public static function redirect($uri)
    {
        header("Location: $uri");
        die();
    }
}