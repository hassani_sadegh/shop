<?php


namespace App\Services\View;


class View
{
    public static function Base($view, $data = array(), $layout = null)
    {
        $view = str_replace(".", DIRECTORY_SEPARATOR, $view);
        $file_path = BASE_PATH . 'views' . DIRECTORY_SEPARATOR . "$view.php";
        if (file_exists($file_path)) {
            extract($data);
            ob_start();
            include_once $file_path;
            $view = ob_get_clean();
            if (is_null($layout)) {
                echo $view;
            } else {
                $layout = str_replace(".", DIRECTORY_SEPARATOR, $layout);
                $layout_path = BASE_PATH . "views" . DIRECTORY_SEPARATOR . "layouts" . DIRECTORY_SEPARATOR . "$layout.php";
                if (file_exists($layout_path)) {
                    include_once $layout_path;
                } else {
                    echo "layout not exist";
                }
            }
        } else {
            echo " Error File : <b style='color: red'>$file_path</b> not exist";
        }
    }


    public static function Theme($view, $data = array(), $layout = null)
    {
        self::Base(ACTIVE_THEME . "." . $view, $data, $layout);
    }
}