<?php

namespace App\Services\Router;

use App\Core\Request;
use App\Services\View\View;

class Router
{
    const baseController = "\\App\\Controllers\\";
    const baseMiddlewares = "\\App\\Middlewares\\";

    public static function start()
    {
        if (self::route_exist()) {
            $request = new Request();
            $allowed_method = self::route_method();
            if (!$request->is_in($allowed_method)) {
                header('HTTP/1.0 403 Forbidden');
                echo "403.php";
                die();
            }

            // check middlewares

            if (self::middleware_exist()) {
                $middlewares = self::get_route_middleware();
                foreach ($middlewares as $middleware) {
                    $middlewares_class = self::baseMiddlewares . $middleware;
                    if (!class_exists($middlewares_class)) {
                        echo "Error :Class <mark>$middlewares_class</mark> not exist";
                        die();
                    }
                    $middleware_object = new $middlewares_class;
                    $middleware_object->handler($request);
                }
            }

            // check controllers
            $targetStr = self::route_target();
            list($control, $method) = explode("@", $targetStr);

            // check class controller
            $control_class = self::baseController . $control;
            if (!class_exists($control_class)) {
                echo "Error : Class <b style='color: #ce0000'>{$control_class}</b> not exist";
                die();
            }
            $controlObj = new $control_class;

            // check method class Controller
            if (!method_exists($control_class, $method)) {
                echo "Error : method <b style='color: #ce0000'>{$method}</b> not exist in class <mark>{$control_class}</mark>";
                die();
            }
            $controlObj->$method($request);
        } else {
            header('HTTP/1.0 404 Not Found');
            View::Base('errors.404');
            die();
        }
    }

    public static function get_all_routes()
    {
        return include BASE_PATH . "routes" . DIRECTORY_SEPARATOR . "web.php";
    }

    public static function current_route()
    {
        $current_route = $_SERVER['REQUEST_URI'];
        return strtok($current_route, "?");
    }

    public static function route_exist()
    {
        $routes = self::get_all_routes();
        $current_route = self::current_route();
        return in_array($current_route, array_keys($routes));
    }

    public static function route_method()
    {
        $routes = self::get_all_routes();
        $current_route = self::current_route();
        $method = $routes[$current_route]['method'];
        return explode("|", $method);
    }

    public static function route_target()
    {
        $routes = self::get_all_routes();
        $current_route = self::current_route();
        return $routes[$current_route]['target'];
    }

    public static function middleware_exist()
    {
        $routes = self::get_all_routes();
        $current_route = self::current_route();
        return isset($routes[$current_route]['middleware']) or !empty(GLOBAL_MIDDLEWARE);
    }

    public static function get_route_middleware()
    {
        $routes = self::get_all_routes();
        $current_route = self::current_route();
        $middlewares = GLOBAL_MIDDLEWARE . "|" . ($routes[$current_route]['middleware'] ?? "");
        return remove_empty_params(explode("|", $middlewares));
    }
}