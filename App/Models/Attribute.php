<?php


namespace App\Models;


use App\Models\Contracts\BaseModel;

class Attribute extends BaseModel
{
    protected $table = "attributes";
    protected $primaryKey = "id";

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'category_attribute',
            'attribute_id',
            'category_id'
        );
    }
}