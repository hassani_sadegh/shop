<?php


namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    protected $table;
    protected $primaryKey;
    public $timestamps = false;
    protected $guarded = ['id'];

    public function findBy($field, $value, $single = true)
    {
        if ($single) {
            return $this->where($field, '=', $value)->first();
        }
        return $this->where($field, '=', $value);
    }
}

