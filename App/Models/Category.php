<?php


namespace App\Models;


use App\Models\Contracts\BaseModel;

class Category extends BaseModel
{
    protected $table = 'categories';
    protected $primaryKey = 'id';

    public function attributes()
    {
        return $this->belongsToMany(
            Attribute::class,
            'category_attribute',
            'category_id',
            'attribute_id'
        );
    }

    public function getAttributeIds($cat_id)
    {
        $cat = $this->find($cat_id);
        if ($cat) {
            return $cat->attributes()->pluck('attribute_id')->toArray();
        }
        return array();
    }

    public function getAttributeName($cat_id)
    {
        $cat = $this->find($cat_id);
        if ($cat) {
            return $cat->attributes()->get();
        }
        return array();
    }

    public function setAttributes($cat_id, $attrs)
    {
        $cat = $this->find($cat_id);
        if ($cat) {
            $cat->attributes()->sync($attrs);
        }
    }
}