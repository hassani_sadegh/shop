<?php


namespace App\Models;


use App\Models\Contracts\BaseModel;

class Product extends BaseModel
{
    protected $table = 'products';
    protected $primaryKey = 'id';
}