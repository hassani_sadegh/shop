<?php


namespace App\Middlewares;

use App\Core\Request;
use App\Middlewares\Contract\BaseMiddleware;

class IEBlocker extends BaseMiddleware
{
    public $agent_key = "Trident/";

    public function handler(Request $request)
    {
        if (strpos($request->agent, $this->agent_key) !== false) {
            echo "<i style='color: brown'>متاسفانه این سایت برروی مرورگر internet explorer اجرا نمیشود</i>";
            die();
        }
    }
}