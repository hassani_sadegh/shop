<?php
return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'market',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => '',
];