<?php
include_once "bootstrap/constant.php";
include_once "vendor/autoload.php";
include_once "helpers/global.php";
include_once "bootstrap/init.php";
\App\Services\Router\Router::start();