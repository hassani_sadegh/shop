<?php
define("BASE_PATH", dirname(__DIR__) . DIRECTORY_SEPARATOR);
define("BASE_URL", "http://market/");
define("IS_DEV_MODE", 1);
define("GLOBAL_MIDDLEWARE", 'IEBlocker');
define("ACTIVE_THEME", 'theme'.DIRECTORY_SEPARATOR);
define("STORAGE", BASE_PATH . 'upload' . DIRECTORY_SEPARATOR);