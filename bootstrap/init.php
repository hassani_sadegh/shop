<?php
if (!session_id()) {
    session_start();
}
// display for developer
if (IS_DEV_MODE) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}

use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;

$capsule = new Capsule;
$capsule->addConnection(get_config('database'));
// Set the event dispatcher used by Eloquent models... (optional)
$capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

//$users = Capsule::table('users')->get();
//echo $users->sum('id');
