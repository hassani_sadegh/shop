<?php
return [
    '/' => [
        'method' => 'get',
        'target' => 'HomeController@index'
    ],

    //admin route
    '/admin' => [
        'method' => 'get',
        'target' => 'Admin\AdminController@index'
    ],

    // category route
    '/admin/categories' => [
        'method' => 'get',
        'target' => 'Admin\CategoryController@index'
    ],
    '/admin/categories/create' => [
        'method' => 'post',
        'target' => 'Admin\CategoryController@create'
    ],
    '/admin/categories/attributes' => [
        'method' => 'get',
        'target' => 'Admin\CategoryController@attribute'
    ],
    '/admin/categories/modify-attribute' => [
        'method' => 'post',
        'target' => 'Admin\CategoryController@modify_attributes'
    ],

    // attributes route
    '/admin/attributes' => [
        'method' => 'get',
        'target' => 'Admin\AttributeController@index'
    ],
    '/admin/attributes/create' => [
        'method' => 'post',
        'target' => 'Admin\AttributeController@create'
    ],

    // product routes
    '/admin/product/list' => [
        'method' => 'get',
        'target' => 'Admin\ProductController@list'
    ],
    '/admin/product/add' => [
        'method' => 'get',
        'target' => 'Admin\ProductController@add'
    ],

];
